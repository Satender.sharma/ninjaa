## System manager ansible ##
=========

we create a role for syetem_manager name 

![main_task_file](/uploads/dc482d6c8f61af3b97df4eaa5d1270b9/main_task_file.png)

## Requirements ##
------------

Operating system:-ubuntu

## Role Variables ##
--------------

![vars_file](/uploads/64be49c5bafed6a352e1640d14cc8587/vars_file.png)

## Dependencies ##
------------

Inventory file 
amazon ec2 machine for worker

## Handler ##
----------------
![handler](/uploads/7e4fd753fd0db4e283bb44242e426ce7/handler.png)

##  Main Playbook ##
![main_master](/uploads/18a93ce4caaf6552e306e3a000fad89a/main_master.png)

## vault ##
ansible-vault create gitmanager.yml

![vault_](/uploads/0c5e555e4a6166a923143d000536fe6d/vault_.png)

## output ##

ansible-playbook -i inventory assignment4.yml --ask-vault-pass

![final_output](/uploads/bebcea8b4e582f5e1bc1d784d9f24db2/final_output.png)


-------



## Author Information ##



![thanks](/uploads/21e97d79533a489c3891af9eb3864444/thanks.jpg)
------------------

