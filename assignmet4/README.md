## System manager ansible ##
=========

we create a role for syetem_manager name 

![system_manager](/uploads/758402b78cf71cff8a73eee320266525/system_manager.png)

## Requirements ##
------------

Operating system:-ubuntu

## Role Variables ##
--------------

![Role_Variables](/uploads/6c2d3ce889eac7584913759941ede567/Role_Variables.png)

## Dependencies ##
------------

Inventory file 
amazon ec2 machine for worker

## Handler ##
----------------
![Handler](/uploads/02899499f1df37602191fb2f6f9bed42/Handler.png)

##  Main Playbook ##
![main_playbook](/uploads/e9d47d7cd6185c7532dac77b51e46d25/main_playbook.png)

## vault ##
ansible-vault create gitmanager.yml

![vault](/uploads/375d171268d1b602fed0c085faa7acef/vault.png)

## output ##

![output](/uploads/92bbdc7cabba9f538e943205efab3a81/output.png)

-------

## Author Information ##

![thank_you](/uploads/3fa1d059fc17e4f7966e148cc3be7c2e/thank_you.jpg)
------------------

